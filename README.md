# Danhoss Security Bundle

## Installation:

1. Register bundle
1. Add `danhoss.security.encryption.method` to your _app/config/parameters.yml_ with one value from array 
returned by `openssl_get_cipher_methods()`
1. Add `danhoss.security.verification.hashes` to your _app/config/parameters.yml_ with structure like: 

~~~
    pair_key_1:
        auth: 'authentication_key_1_here'
        encrypt: 'encription_key_1_here'
    pair_key_2:
        auth: 'authentication_key_2_here'
        encrypt: 'encription_key_2_here'
    ...
~~~


In case of encryption one of pairs must be named `default`. When decrypting own data use `"default"` 
or `DanhossSecurityInterface::OWN_SOURCE_KEY` as source parameter. 
- - -

## Usage:
~~~
$danhossSecurityService = $container->get('danhoss.security');
$danhossSecurityService->setEncryptionMethod('aes-128-cbc-hmac-sha256');

list($encryptedData, $checksum) = $danhossSecurityService->encrypt(['your_data_here']);

$decryptedData = $danhossSecurityService->decrypt('default', $encryptedData, $checksum);
~~~
All methods can throw `\Danhoss\CypherBundle\Exception\DanhossSecurityException` with appropriate message and code.

- - -

## Changelog: 

### 2017.12.29
* Repository init