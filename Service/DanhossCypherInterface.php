<?php
/**
 *
 * @since 29.12.17 12:54
 * @author Marcin Czajkowski <mczajkowski@danhoss.com>
 */

namespace Danhoss\CypherBundle\Service;


interface DanhossCypherInterface
{
    /** @var string */
    const OWN_SOURCE_KEY = 'default';

}