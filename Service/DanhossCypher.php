<?php
/**
 * @since 2017-12-29 11:54
 *
 * @author Marcin Czajkowski <mczajkowski@danhoss.com>
 */

namespace Danhoss\CypherBundle\Service;

use Danhoss\CypherBundle\Exception\DanhossCypherException;

/**
 * Class DanhossSecurity.php
 */
class DanhossCypher implements DanhossCypherInterface
{
    /** @var array */
    protected $verificationHashes;

    /** @var string */
    protected $encryptionMethod;

    /**
     * @param array $hashes
     */
    public function setVerificationHashes(array $hashes)
    {
        $this->verificationHashes = $hashes;
    }

    /**
     * @param string $method
     */
    public function setEncryptionMethod(string $method)
    {
        $this->encryptionMethod = $method;
    }

    /**
     * @param string $source
     * @param string $data
     * @param string $checksum
     *
     * @throws DanhossCypherException
     *
     * @return array
     */
    public function decrypt(string $source, string $data, string $checksum)
    {
        $this->validateSource($source);
        $decryptedData = $this->decryptData($source, $data);
        $this->validateRequest($source, $checksum, $decryptedData);

        $parsedData = $this->parseDecryptedData($decryptedData);

        return $parsedData;
    }

    /**
     * @param array $data
     *
     * @return array
     * @throws DanhossCypherException
     */
    public function encrypt(array $data)
    {
        $source = DanhossCypherInterface::OWN_SOURCE_KEY;

        $encodedData = json_encode($data);
        $checksum = $this->generateChecksum($this->verificationHashes[$source]['auth'], $encodedData);
        $encryptedData = $this->encryptData($source, $encodedData);

        return [$encryptedData, $checksum];
    }

    // ~

    /**
     * @param string $source
     *
     * @throws DanhossCypherException
     *
     * @return $this
     */
    protected function validateSource(string $source)
    {
        if (false == isset($this->verificationHashes[$source])) {
            throw new DanhossCypherException('Source service is invalid or not defined.', DanhossCypherException::EXCEPTION_CODE_SOURCE_INVALID);
        }

        return $this;
    }

    /**
     * @param $source
     * @param $data
     *
     * @throws DanhossCypherException
     *
     * @return string
     */
    protected function decryptData(string $source, string $data)
    {
        $encryptHash = $this->verificationHashes[$source]['encrypt'];

        try {
            $iv_length = openssl_cipher_iv_length($this->encryptionMethod);

            $iv = substr($data, 0, $iv_length);
            $data = substr($data, $iv_length);

            $decryptedData = openssl_decrypt($data, $this->encryptionMethod, $encryptHash, 0, $iv);
        } catch (\Exception $exception) {
            throw new DanhossCypherException(
                sprintf('Could not decrypt data: %s', $exception->getMessage()),
                DanhossCypherException::EXCEPTION_CODE_DECRYPTION_FAIL,
                $exception
            );
        }

        return $decryptedData;
    }

    /**
     * @param $source
     * @param $data
     *
     * @throws DanhossCypherException
     *
     * @return string
     */
    protected function encryptData(string $source, string $data)
    {
        $encryptHash = $this->verificationHashes[$source]['encrypt'];

        try {
            $iv_size = openssl_cipher_iv_length($this->encryptionMethod);
            $base = str_shuffle(MD5(microtime()));

            while (strlen($base) < $iv_size) {
                $base = $base . str_shuffle($base);
            }

            $iv = substr($base, 0, $iv_size);

            $encryptedData = $iv . openssl_encrypt($data, $this->encryptionMethod, $encryptHash, 0, $iv);
        } catch (\Exception $exception) {
            throw new DanhossCypherException(
                sprintf('Could not encrypt data: %s', $exception->getMessage()),
                DanhossCypherException::EXCEPTION_CODE_ENCRYPTION_FAIL,
                $exception
            );
        }

        return $encryptedData;
    }

    /**
     * @param string $source
     * @param string $checksum
     * @param string $data
     *
     * @throws DanhossCypherException
     *
     * @return $this
     */
    protected function validateRequest(string $source, string $checksum, string $data)
    {
        $validationHash = $this->generateChecksum($this->verificationHashes[$source]['auth'], $data);

        if ($validationHash != $checksum) {
            throw new DanhossCypherException('Authentication failed. Checksum does not match', DanhossCypherException::EXCEPTION_CODE_VALIDATION_FAIL);
        }

        return $this;
    }

    /**
     * @param string $decryptedData
     *
     * @return mixed
     */
    protected function parseDecryptedData(string $decryptedData)
    {
        try {
            $data = json_decode($decryptedData, true);
        } catch (\Exception $exception) {
            $data = false;
        }
        
        if (false == $data) {
            $data = $decryptedData;
        }

        return $data;
    }

    /**
     * @param string $authHash
     * @param string $data
     *
     * @return string
     */
    protected function generateChecksum(string $authHash, string $data)
    {
        $authString = $data . $authHash;
        $hash = sha1($authString);

        return $hash;
    }
}
