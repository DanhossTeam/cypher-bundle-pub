<?php
/**
 *
 * @since 29.12.17 11:49
 * @author Marcin Czajkowski <mczajkowski@danhoss.com>
 */

namespace Danhoss\CypherBundle\Exception;


/**
 * Class DanhossSecurityException.php
 * @package Danhoss\CypherBundle\Exception
 */

class DanhossCypherException extends \Exception
{
    /** @var int */
    const EXCEPTION_CODE_SOURCE_INVALID = 1;

    /** @var int */
    const EXCEPTION_CODE_VALIDATION_FAIL = 2;

    /** @var int */
    const EXCEPTION_CODE_ENCRYPTION_FAIL = 3;

    /** @var int */
    const EXCEPTION_CODE_DECRYPTION_FAIL = 4;
}